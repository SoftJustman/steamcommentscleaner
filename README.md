*Created specially for [miped.ru](http://miped.ru)*

*[Guide on Steam](http://steamcommunity.com/sharedfiles/filedetails/?id=1124166890)*

# Steam Comments Cleaner

This JS code deletes all deletable comments posted from this account

**How to use:**

1. Follow [this link](http://steamcommunity.com/my/commenthistory)

2. Set custom filters *(if needed)*

3. Open developer console *(Ctrl+Shift+J or F12)*

4. Place code from script.js into console 

5. You can change range of pages in *Changable constants*

6. Execute script *(Enter)*

7. Wait until "Done"